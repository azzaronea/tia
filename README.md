tia
==============================

Twitter Ideology + Computer Vision project

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    |   ├── shared         <- Data share between experiments
    │   ├── experiments
    |   |   ├── 20170703...  <- Experiment directory
    |   |   |   ├── raw         <- Link
    |   |   |   ├── external    <- Link
    |   |   |   ├── interim     <- Intermediate data that has been transformed.
    |   |   |   ├── processed   <- The final, canonical data sets for modeling.
    |   |   |   └── shared      <- Shared
    |   |   |
    |   |   └── current      <- Link to the current experiment directory
    |   |
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │   ├── experiments
    |   |   ├── 20170703...  <- Experiment directory
    |   |   |   ├── external <- Link
    |   |   └── current      <- Link to the current experiment directory
    │   └── external         <- External pretrained models
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


Steps
------------
1. Download data from [here](https://www.dropbox.com/sh/da7f4ov4tflcf8u/AAAlNHE_ttS8iJz30UTcK-H3a?dl=0) and put in data/raw directory
2. $ make create_enviroment
3. Follow the steps in the output of above command
4. Enter virtualenv
5. $ make new_experiment
6. $ make train_nlp
