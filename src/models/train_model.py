# -*- coding: utf-8 -*-
import os
import click
from dotenv import find_dotenv, load_dotenv
import logging

from common import *
os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils


@click.command()
@click.argument('datadir', type=click.Path(exists=True))
@click.argument('modeldir', type=click.Path(exists=True))
@click.argument('reportdir', type=click.Path(exists=True))
@click.argument('force', default=False)
def main(datadir, modeldir, reportdir, force):
    logger = logging.getLogger(__name__)

    logger.info('Trying to load processed data...')
    tdm = utils.load_pickle(os.path.join(datadir, 'processed', 'term-document-matrix.pkl'))
    labels = utils.load_pickle(os.path.join(datadir, 'processed', 'labels.pkl'))
    indices = utils.load_pickle(os.path.join(datadir, 'processed', 'indices.pkl'))
    vocab = utils.load_pickle(os.path.join(datadir, 'processed', 'vocab.pkl'))
    metadata = utils.load_pickle(os.path.join(datadir, 'interim', 'metadata.pkl'))

    labels_code = map(int, labels[:, 0])
    labels_name = labels[:, 1]

    logger.info('Training...')
    grid_serach(tdm, labels_code, indices, vocab, metadata,
                rpath=os.path.join(reportdir, 'normal'),
                opath=os.path.join(modeldir, 'normal'))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
