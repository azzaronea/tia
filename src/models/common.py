# -*- coding: utf-8 -*-
import codecs
import csv
import os
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.metrics import classification_report

os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils


def grid_serach(X, y, idx, vocab, metadata, rpath, opath):
    seed = int(os.environ.get('RANDOM_SEED'))
    clfs = os.environ.get('CLFS').split(',')
    test_sizes = [float(ts) for ts in os.environ.get('TEST_SIZES').split(',')]
    iters = int(os.environ.get('ITERS'))

    meta_header = []
    data = []
    twords = {}
    adjust = {}
    for clf in clfs:
        for test_size in test_sizes:
            for iter in range(iters):
                clf_meta = [clf, test_size, iter]
                clf_header = ['Classifier', 'SamplePct', 'Iteration']

                print("Doing: " + clf + " iter: " + str(iter))

                XTrain, XTest, yTrain, yTest, idxTrain, idxTest = train_test_split(
                    X, y, idx, test_size=test_size, random_state=seed * iter)

                cl, yPred = classify(XTrain, yTrain, XTest, yTest, clf)
                report = classification_report(yTest, np.argmax(yPred, axis=1))
                with open(os.path.join(rpath, 'classification_report_' + clf + '_' + str(iter)), 'w') as of:
                    of.write(report)

                utils.dump_pickle(cl, os.path.join(opath, clf + str(iter) + 'clf.pkl'))

                res, adj = clean_results(yPred, idxTest)
                if clf == 'nb':
                    twords[iter] = top_words(vocab, cl, n=200)
                adjust[clf + '-' + str(iter)] = adj
                for k, r in res.iteritems():
                    meta_header = metadata[k].keys()
                    data.append([k] + metadata[k].values() + clf_meta + r)

    save_results(data, meta_header, clf_header, os.path.join(rpath, 'results.csv'))

    with open(os.path.join(rpath, 'Essay_StandardizeValues.csv'),'w') as f:
        f.write('Classifier,Iteration,Mean,Std\n')
        for fn,val in adjust.iteritems():
            f.write(fn.replace('-',',')+','+','.join(map(str,val))+'\n')

    with codecs.open(os.path.join(rpath,'FinalPartisanWords.csv'),'w', 'utf-8') as f:
        f.write('Iteration,Word,IsRepub,RepScore,DemScore\n')
        for it, ws in twords.iteritems():
            for w,scores in ws.iteritems():
                f.write(str(it)+","+w+','+','.join(map(str,scores))+'\n')


def top_words(vocab, clf, n=20):
    topWords = {}
    coefs_with_fns = sorted(zip(clf.coef_[0], vocab))
    top = coefs_with_fns[:-(n + 1):-1]
    for w in top:
        word = w[1]
        for j, f in enumerate(vocab):
            if f == word:
                demscore = np.exp(clf.feature_log_prob_[0][j])
                repscore = np.exp(clf.feature_log_prob_[1][j])
                repflag = 1 * (repscore > demscore)
                topWords[word] = [repflag, repscore, demscore]
    return topWords


def classify(XTrain, yTrain, XTest, yTest, clf):
    if clf == 'knn':
        cl = KNeighborsClassifier()
        cl.fit(XTrain, yTrain)
        pred = cl.predict_proba(XTest)
    elif clf == 'svm':
        cl = SVC(C=100, gamma=.1, probability=True)
        cl.fit(XTrain, yTrain)
        pred = cl.predict_proba(XTest)
    elif clf == 'nb':
        cl = MultinomialNB()
        cl.fit(XTrain, yTrain)
        pred = cl.predict_proba(XTest)

    return cl, pred


def clean_results(yPred, idxTest):
    res = dict(zip(idxTest, [[] for i in range(len(idxTest))]))

    yPred[yPred == 1] = np.max(yPred[yPred < 1])
    yPred[yPred == 0] = np.min(yPred[yPred > 0])

    for i, line in enumerate(yPred):
        res[idxTest[i]] = [line[0], line[1], np.log(line[0] / line[1])]

    vals = [i[2] for i in res.values()]
    m = np.mean(vals)
    sd = np.std(vals)
    for k, v in res.items():
        res[k] = [v[0], v[1], (v[2] - m) / sd]
    return res, [m, sd]

def save_results(data,meta_header,clf_header,outfile=''):
    header=['username']+meta_header+clf_header+['DemProb','RepProb','zLkRatio']
    f= open(outfile,'wb')
    writeit=csv.writer(f)
    writeit.writerow(header)
    for line in data:
        writeit.writerow(line)
    f.close()

    return