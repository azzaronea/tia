import os
from tempfile import mkstemp
import unittest

import utils


class TestUtils(unittest.TestCase):

    def test_is_non_zero_file(self):
        self.assertFalse(utils.is_non_zero_file("/no-existing-file"))
        _, tpath = mkstemp()
        self.assertFalse(utils.is_non_zero_file(tpath))
        self.assertTrue(utils.is_non_zero_file(__file__))

    def test_num_tweets(self):
        fpath = os.path.join(os.path.dirname(__file__), 'test_data', 'tweets.txt')
        self.assertEqual(4, utils.num_tweets(fpath))

    def test_pikle(self):
        obj = [1, 2, 3]
        _, tpath = mkstemp()
        utils.dump_pickle(obj, tpath)
        obj2 = utils.load_pickle(tpath)
        self.assertEqual(obj, obj2)

    def test_add_extracted_text_to_tweet(self):
        tweet = {'id': 1, 'text': 'This is a tweet.', 'extra-field': '123'}
        use_img = os.path.join(os.path.dirname(__file__), 'test_data')
        tweet = utils.add_extracted_text_to_tweet(tweet, use_img)
        self.assertEqual(tweet['text'], 'This is a tweet.\nextracted text.')
        self.assertEqual(tweet['id'], 1)
        self.assertEqual(tweet['extra-field'], '123')

        tweet = {'id': 2, 'text': 'This is a tweet.', 'extra-field': '123'}
        use_img = os.path.join(os.path.dirname(__file__), 'test_data')
        tweet = utils.add_extracted_text_to_tweet(tweet, use_img)
        self.assertEqual(tweet['text'], 'This is a tweet.')
        self.assertEqual(tweet['id'], 2)
        self.assertEqual(tweet['extra-field'], '123')

    def test_read_json_dataset(self):
        path = os.path.join(os.path.dirname(__file__), 'test_data', 'tweets.txt')
        tweets = [t for t in utils.read_json_dataset(path)]
        self.assertEqual(4, len(tweets))
        self.assertEqual(tweets[0]['id'], 1187280774)
        self.assertEqual(tweets[1]['id'], 1638489615)
        self.assertEqual(tweets[2]['id'], 1638502363)
        self.assertEqual(tweets[3]['id'], 191204571782189056)
        self.assertEqual('has just joined after hearing celbs talk bout it wth do u do ?', tweets[0]['text'])

        use_img = os.path.join(os.path.dirname(__file__), 'test_data')
        tweets = [t for t in utils.read_json_dataset(path, use_img)]
        self.assertEqual(4, len(tweets))
        self.assertEqual(tweets[0]['id'], 1187280774)
        self.assertEqual(tweets[1]['id'], 1638489615)
        self.assertEqual(tweets[2]['id'], 1638502363)
        self.assertEqual(tweets[3]['id'], 191204571782189056)
        self.assertEqual(
            'has just joined after hearing celbs talk bout it wth do u do ?\nextracted test', tweets[0]['text'])


if __name__ == '__main__':
    unittest.main()
