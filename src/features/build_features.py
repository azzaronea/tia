# -*- coding: utf-8 -*-
import os
import click
from dotenv import find_dotenv, load_dotenv
import logging

import common
os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils


@click.command()
@click.argument('datadir', type=click.Path(exists=True))
@click.argument('modeldir', type=click.Path(exists=True))
@click.argument('force', default=False)
def main(datadir, modeldir, force):
    logger = logging.getLogger(__name__)

    metadata = None
    if not force:
        logger.info('Trying to load metadata...')
        metadata = utils.load_pickle(os.path.join(datadir, 'interim', 'metadata.pkl'))

    if not metadata:
        logger.info('Metadata not found. Building them...')
        metadata = common.init_metadata(os.path.join(datadir, 'raw', 'users.json'),
                                        os.path.join(datadir, 'raw', 'politicians.csv'))
        logger.info('Dumping metadata...')
        utils.dump_pickle(metadata, os.path.join(datadir, 'interim', 'metadata.pkl'))

    essays = None
    if not force:
        logger.info('Trying to load essays...')
        essays = utils.load_pickle(os.path.join(datadir, 'interim', 'essays.pkl'))

    if not essays:
        logger.info('Essays not found. Building them...')
        essays = common.make_essays(os.path.join(datadir, 'interim', 'tweets'), metadata)
        logger.info('Dumping essays...')
        utils.dump_pickle(essays, os.path.join(datadir, 'interim', 'essays.pkl'))
        logger.info('Dumping metadata...')
        utils.dump_pickle(metadata, os.path.join(datadir, 'interim', 'metadata.pkl'))

    tdm = None
    labels = None
    indices = None
    vocab = None
    if not force:
        logger.info('Trying to load vectors...')
        tdm = utils.load_pickle(os.path.join(datadir, 'processed', 'term-document-matrix.pkl'))
        labels = utils.load_pickle(os.path.join(datadir, 'processed', 'labels.pkl'))
        indices = utils.load_pickle(os.path.join(datadir, 'processed', 'indices.pkl'))
        vocab = utils.load_pickle(os.path.join(datadir, 'processed', 'vocab.pkl'))

    if tdm is None or labels is None or indices is None or vocab is None:
        logger.info('Vectorizing...')
        vectorizer, tdm, labels, indices = common.vectorize(essays, metadata)
        logger.info('Dumping vectors...')
        utils.dump_pickle(tdm, os.path.join(datadir, 'processed', 'term-document-matrix.pkl'))
        utils.dump_pickle(labels, os.path.join(datadir, 'processed', 'labels.pkl'))
        utils.dump_pickle(indices, os.path.join(datadir, 'processed', 'indices.pkl'))
        utils.dump_pickle(vectorizer.get_feature_names(), os.path.join(datadir, 'processed', 'vocab.pkl'))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
