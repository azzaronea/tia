# from datetime import datetime
# import os
# from tempfile import mkstemp, mkdtemp
import unittest

from common import *


class TestUtils(unittest.TestCase):

    def test_is_cite(self):
        self.assertFalse(is_cite(''))
        self.assertFalse(is_cite(None))
        self.assertFalse(is_cite('name'))
        self.assertTrue(is_cite('@name'))
        self.assertFalse(is_cite('n@ame'))

    def test_is_hashtag(self):
        self.assertFalse(is_hashtag(''))
        self.assertFalse(is_hashtag(None))
        self.assertFalse(is_hashtag('name'))
        self.assertTrue(is_hashtag('#name'))
        self.assertFalse(is_hashtag('n#ame'))

    def test_is_url(self):
        self.assertFalse(is_url(''))
        self.assertFalse(is_url(None))
        self.assertFalse(is_url('name'))
        self.assertTrue(is_url('http://url'))
        self.assertTrue(is_url('https://url'))
        self.assertFalse(is_url('httpsss://url'))

    def test_init_metadata(self):
        ipath = os.path.join(os.path.dirname(__file__), 'test_data', 'users.json')
        ppath = os.path.join(os.path.dirname(__file__), 'test_data', 'politicians.csv')
        metadata = init_metadata(ipath, ppath)
        self.assertEqual(3, len(metadata))
        self.assertIn('screen_name1', metadata)
        self.assertIn('screen_name2', metadata)
        self.assertIn('screen_name3', metadata)
        self.assertEqual(metadata['screen_name1']['id_str'], "1")
        self.assertEqual(metadata['screen_name2']['id_str'], "2")
        self.assertEqual(metadata['screen_name3']['id_str'], "3")
        self.assertEqual(metadata['screen_name1']['name'], "name1")
        self.assertEqual(metadata['screen_name2']['name'], "name2")
        self.assertEqual(metadata['screen_name3']['name'], "name3")
        self.assertEqual(metadata['screen_name1']['party'], "Independent")
        self.assertEqual(metadata['screen_name2']['party'], "Democratic")
        self.assertEqual(metadata['screen_name3']['party'], "Republican")
        self.assertNotEquals(metadata['screen_name1']['partyCode'],
                             metadata['screen_name2']['partyCode'],
                             metadata['screen_name3']['partyCode'])
        self.assertEqual(0, metadata['screen_name1']['num_tweets'])
        self.assertEqual(0, metadata['screen_name2']['num_tweets'])
        self.assertEqual(0, metadata['screen_name3']['num_tweets'])
        self.assertEqual(0, metadata['screen_name1']['num_words'])
        self.assertEqual(0, metadata['screen_name2']['num_words'])
        self.assertEqual(0, metadata['screen_name3']['num_words'])

    def test_preprocess_tweet1(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "1"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet #htignored.'}
        self.assertEqual(preprocess_tweet(tweet), [])
        tweet = {'text': 'This is a simple tweet #HT1 #ht2.',
                 'entities': {'hashtags': [{'text': 'HT1'}, {'text': 'ht2'}]}}
        self.assertEqual(preprocess_tweet(tweet), ['ht1', 'ht2'])

    def test_preprocess_tweet2(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "1"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "1"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet #HT1 #ht2.',
                 'entities': {'hashtags': [{'text': 'HT1'}, {'text': 'ht2'}]}}
        self.assertEqual(preprocess_tweet(tweet), ['HT1', 'ht2'])

    def test_preprocess_tweet3(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = "HLINK"
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet http://url and https://url2'}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet', 'HLINK', 'and', 'HLINK'], preprocess_tweet(tweet))

    def test_preprocess_tweet4(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet http://url and https://url2'}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet', 'and'], preprocess_tweet(tweet))

    def test_preprocess_tweet5(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet #ht1 and #ht2'}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet', '#ht1', 'and', '#ht2'], preprocess_tweet(tweet))

    def test_preprocess_tweet6(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = "HTAG"
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet #ht1 and #ht2'}
        self.assertEqual(
            ['this', 'is', 'a', 'simple', 'tweet', 'HTAG_ht1', 'and', 'HTAG_ht2'], preprocess_tweet(tweet))

    def test_preprocess_tweet7(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet @cite1 and @cite2'}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet', '@cite1', 'and', '@cite2'], preprocess_tweet(tweet))

    def test_preprocess_tweet8(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = "AT"
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet @cite1 and @cite2'}
        self.assertEqual(
            ['this', 'is', 'a', 'simple', 'tweet', 'AT_cite1', 'and', 'AT_cite2'], preprocess_tweet(tweet))

    def test_preprocess_tweet9(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet.', 'retweeted': False}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet'], preprocess_tweet(tweet))
        tweet = {'text': 'This is a simple tweet.', 'retweeted': True}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet'], preprocess_tweet(tweet))

    def test_preprocess_tweet10(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = "RT"

        tweet = {'text': 'This is a simple tweet.', 'retweeted': False}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet'], preprocess_tweet(tweet))
        tweet = {'text': 'This is a simple tweet.', 'retweeted': True}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet', 'RT'], preprocess_tweet(tweet))

    def test_preprocess_tweet11(self):
        os.environ['PREPROCESS_STOPWORDS'] = "1"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet.'}
        self.assertEqual(['simple', 'tweet'], preprocess_tweet(tweet))

    def test_preprocess_tweet12(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "0"
        os.environ['PRESEVE_PUNCTUATION'] = "1"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple tweet.'}
        self.assertEqual(['this', 'is', 'a', 'simple', 'tweet', '.'], preprocess_tweet(tweet))

    def test_preprocess_tweet13(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "1"
        os.environ['PRESEVE_PUNCTUATION'] = "0"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple Tweet.'}
        self.assertEqual(['This', 'is', 'a', 'simple', 'Tweet'], preprocess_tweet(tweet))

    def test_preprocess_tweet14(self):
        os.environ['PREPROCESS_STOPWORDS'] = "0"
        os.environ['PRESERVE_CASE'] = "1"
        os.environ['PRESEVE_PUNCTUATION'] = "1"
        os.environ['ONLY_HASHTAGS'] = "0"
        os.environ['HLINK_TOKEN'] = ""
        os.environ['CITE_PREFIX'] = ""
        os.environ['HASHTAG_PREFIX'] = ""
        os.environ['RETWEET_TOKEN'] = ""

        tweet = {'text': 'This is a simple Tweet.'}
        self.assertEqual(['This', 'is', 'a', 'simple', 'Tweet', '.'], preprocess_tweet(tweet))

    def test_make_essay(self):
      os.environ['PREPROCESS_STOPWORDS'] = "1"

      ipath = os.path.join(os.path.dirname(__file__), 'test_data', 'users.json')
      ppath = os.path.join(os.path.dirname(__file__), 'test_data', 'politicians.csv')
      metadata = init_metadata(ipath, ppath)
      ipath = os.path.join(os.path.dirname(__file__), 'test_data', 'tweets')
      essays = make_essays(ipath, metadata)
      self.assertEqual([u'first', u'tweet', u'second', u'tweet', u'third', u'tweet'], essays['screen_name1'])
      self.assertEqual([u'text', u'one', u'text', u'two', u'text', u'three'], essays['screen_name2'])
      self.assertEqual([u'yellow', u'one', u'yellow', u'two', u'yellow', u'three'], essays['screen_name3'])
      self.assertEqual(3, metadata['screen_name1']['num_tweets'])
      self.assertEqual(3, metadata['screen_name2']['num_tweets'])
      self.assertEqual(3, metadata['screen_name3']['num_tweets'])
      self.assertEqual(6, metadata['screen_name1']['num_words'])
      self.assertEqual(6, metadata['screen_name2']['num_words'])
      self.assertEqual(6, metadata['screen_name3']['num_words'])


    def test_make_essay(self):
      os.environ['PREPROCESS_STOPWORDS'] = "1"

      ipath = os.path.join(os.path.dirname(__file__), 'test_data', 'users.json')
      ppath = os.path.join(os.path.dirname(__file__), 'test_data', 'politicians.csv')
      metadata = init_metadata(ipath, ppath)
      ipath = os.path.join(os.path.dirname(__file__), 'test_data', 'tweets')
      use_img = os.path.join(os.path.dirname(__file__), 'test_data', 'img2text')
      essays = make_essays(ipath, metadata, use_img)
      self.assertEqual([u'first', u'tweet', 'extracted', 'text', u'second', u'tweet', u'third', u'tweet'], essays['screen_name1'])
      self.assertEqual([u'text', u'one', u'text', u'two', u'text', u'three'], essays['screen_name2'])
      self.assertEqual([u'yellow', u'one', u'yellow', u'two', u'yellow', u'three'], essays['screen_name3'])
      self.assertEqual(3, metadata['screen_name1']['num_tweets'])
      self.assertEqual(3, metadata['screen_name2']['num_tweets'])
      self.assertEqual(3, metadata['screen_name3']['num_tweets'])
      self.assertEqual(8, metadata['screen_name1']['num_words'])
      self.assertEqual(6, metadata['screen_name2']['num_words'])
      self.assertEqual(6, metadata['screen_name3']['num_words'])

if __name__ == '__main__':
    unittest.main()
