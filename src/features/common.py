# -*- coding: utf-8 -*-
import os
import numpy as np
import nltk
import pandas
from sklearn.feature_extraction.text import TfidfVectorizer as tfidf
import string
from tqdm import tqdm

os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils


def init_metadata(ipath, ppath):
    metadata = {}

    p_df = pandas.read_csv(ppath)
    p_df.set_index('TwitterName', inplace=True)

    for user in utils.read_json_dataset(ipath):
        sname = user['screen_name']

        if sname not in p_df.index:
            continue

        metadata[sname] = {
            'id_str': user['id_str'],
            'name':  user['name'],
            'party': p_df.loc[sname]['Party'],
            'partyCode': 0 if p_df.loc[sname]['Party'] == "Democratic" else 1,
            'num_tweets': 0,
            'num_words': 0
        }

    return metadata


def make_essays(ipath, metadata, use_img=None):
    essays = {}
    for file in tqdm(os.listdir(ipath)):
        # skips tweets if we don't have metadata about the user
        if file not in metadata:
            continue

        essays[file] = []

        with tqdm(total=utils.num_tweets(os.path.join(ipath, file))) as pbar:
            for tweet in utils.read_json_dataset(os.path.join(ipath, file), use_img):
                pbar.update(1)
                sname = str(tweet['screen_name'])
                assert(file == sname)

                ptweet = preprocess_tweet(tweet)
                essays[sname].extend(ptweet)
                metadata[sname]['num_words'] += len(ptweet)
                metadata[sname]['num_tweets'] += 1

    return essays


def preprocess_tweet(tweet):
    remove_stopwords = os.environ.get('PREPROCESS_STOPWORDS') == "1"
    preserve_case = os.environ.get('PRESERVE_CASE') == "1"
    preserve_punct = os.environ.get('PRESEVE_PUNCTUATION') == "1"
    only_hashtags = os.environ.get('ONLY_HASHTAGS') == "1"
    hlink_token = os.environ.get('HLINK_TOKEN')
    cprefix = os.environ.get('CITE_PREFIX')
    hprefix = os.environ.get('HASHTAG_PREFIX')
    retweet_token = os.environ.get('RETWEET_TOKEN')

    if remove_stopwords:
        stopwords = nltk.corpus.stopwords.words('english')
        stopwords += ['bit', 'fb', 'ow', 'twitpic', 'ly',
                      'com', 'rt', 'http', 'tinyurl', 'nyti', 'www']
    else:
        stopwords = []

    if only_hashtags:
        try:
          hts = [ht['text'] for ht in tweet['entities']['hashtags']]
        except:
          hts = []
        return hts if preserve_case else [ht.lower() for ht in hts]
    else:
        # Tokenize
        tokenizer = nltk.tokenize.TweetTokenizer(preserve_case, reduce_len=True, strip_handles=False)
        tokens = tokenizer.tokenize(tweet['text'])

        # Remove or replace links with HLINK_TOKEN
        tokens = list(map(lambda t: hlink_token if is_url(t) else t, tokens))

        # Replace hastags with ${HASHTAG_PREFIX}_hashtag
        if hprefix:
            tokens = list(map(lambda t: hprefix + '_' + t[1:] if is_hashtag(t) else t, tokens))

        # Replace tags with ${CITE_PREFIX}_hashtag
        if cprefix:
            tokens = list(map(lambda t: cprefix + '_' + t[1:] if is_cite(t) else t, tokens))

        # Filter out None
        tokens = [t for t in tokens if t is not None]

        # If needed append retweet token
        if 'retweeted' in tweet and tweet['retweeted'] and retweet_token:
            tokens.append(retweet_token)

        # Remove stopworkds
        tokens = list(filter(lambda t: t not in stopwords, tokens))

        # Remove punctuation
        if not preserve_punct:
            tokens = list(filter(lambda t: t not in string.punctuation, tokens))

        return tokens


def is_url(token):
    return (token and (token.startswith('http://') or token.startswith('https://')))


def is_hashtag(token):
    return token and token.startswith('#')


def is_cite(token):
    return token and token.startswith('@')


def vectorize(essays, metadata):
    ngram_max = int(os.environ.get('NGRAM_MAX'))
    min_df = int(os.environ.get('TFIDF_CUTOFF'))

    def dumb_preprocessor(s):
        return s

    def dumb_tokenizer(s):
        return s

    vectorizer = tfidf(list(essays.values()), preprocessor=dumb_preprocessor,
                       tokenizer=dumb_tokenizer, ngram_range=(1, ngram_max), min_df=min_df, lowercase=False)
    tdm = vectorizer.fit_transform(tqdm(list(essays.values())))

    labels = [[int(metadata[k]['partyCode']), metadata[k]['party']] for k in essays.keys()]

    return vectorizer, tdm, np.array(labels), list(essays.keys())