# -*- coding: utf-8 -*-
import os
import codecs
import json
import pickle


def is_non_zero_file(fpath):
    return os.path.isfile(fpath) and os.path.getsize(fpath) > 0


def num_tweets(fpath):
    return sum(1 for line in codecs.open(fpath, 'r', 'utf-8') if len(line.strip()) > 0)


def add_extracted_text_to_tweet(tweet, use_img):
    fpath = os.path.join(use_img, str(tweet['id']))
    if is_non_zero_file(fpath):
        with codecs.open(fpath, 'r', encoding='utf-8') as f:
            tweet['text'] += u'\n' + f.read()
    return tweet


def read_json_dataset(path, use_img=None):
    with codecs.open(path, 'r', 'utf-8') as data_file:
        for line in data_file:
            tweet = json.loads(line)
            if use_img is not None:
                tweet = add_extracted_text_to_tweet(tweet, use_img)
            yield tweet


def load_pickle(ifile):
    if not os.path.exists(ifile):
        return None
    return pickle.load(open(ifile, 'rb'))


def dump_pickle(obj, ofile):
    pickle.dump(obj, open(ofile, 'wb'))
