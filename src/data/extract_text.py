# -*- coding: utf-8 -*-
import cv2
import os
import click
import io
import logging
from dotenv import find_dotenv, load_dotenv
from tqdm import tqdm
import glob

from text_box_detector import TextBoxDetector
from text_page import TextPage

os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils


def extract_text(input_path, images_path, output_path, tbd):
    if not os.path.isdir(output_path):
        os.mkdir(output_path)

    for file in tqdm(os.listdir(input_path)):
        with tqdm(total=utils.num_tweets(os.path.join(input_path, file))) as pbar:
            for tweet in utils.read_json_dataset(os.path.join(input_path, file)):
                pbar.update(1)

                output_file = os.path.join(output_path, str(tweet['id']))
                if os.path.exists(output_file):
                    continue

                with io.open(output_file, 'w', encoding='utf-8') as of:
                    for img_path in glob.glob(os.path.join(images_path, str(tweet['id']) + '*')):
                        im = cv2.imread(img_path)
                        page = TextPage(tbd, im)
                        for block in page.detect_blocks(is_display=False):
                            of.write(unicode(block))


@click.command()
@click.argument('datadir', type=click.Path(exists=True))
def main(datadir):
    logger = logging.getLogger(__name__)

    project_dir = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)

    logger.info('Loading text detector model...')
    tbd = TextBoxDetector()
    tbd.load(os.path.join(project_dir, 'models', 'external', 'ctpn'), use_gpu=True)

    logger.info('Extracting text...')
    extract_text(os.path.join(datadir, 'interim', 'tweets'),
                 os.path.join(datadir, 'shared', 'img'),
                 os.path.join(datadir, 'shared', 'img2text'),
                 tbd)
    logger.info('Text extracted')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
