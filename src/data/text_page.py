import cv2
from matplotlib import pyplot as plt
import numpy as np
import skimage.measure
from tesserocr import PyTessBaseAPI, PSM, OEM
from PIL import Image
from math import *

from text_line import TextLine
from text_block import TextBlock


class TextPage:

    def __init__(self, text_box_detector, image):
        self.tbd = text_box_detector
        self.image = image

    def detect_blocks(self, is_display=False):
        if self.image is None:
            return []

        temp_image = np.zeros_like(self.image[:, :, 0])

        lines = self.detect_lines(is_display)
        for line in lines:
            scaled_line = line.scale(10)
            rr, cc = scaled_line.polygon(clip=temp_image.shape)
            temp_image[rr, cc] = 255

        if is_display:
            plt.figure(figsize=(10, 10))
            plt.imshow(temp_image)
            plt.show()

        ll, num_ll = skimage.measure.label(
            temp_image, background=0, return_num=True, connectivity=1)

        if is_display:
            plt.figure(figsize=(10, 10))
            plt.imshow(temp_image * ll / 255)
            plt.show()

        blocks = [TextBlock() for i in range(num_ll)]
        for line in lines:
            block_id = ll[line.centroid()] - 1
            blocks[block_id].add_line(line)
        return blocks

    def detect_lines(self, is_display=False):
        raw_lines, im_out = self.tbd.getBoxes(self.image)
        raw_lines = np.clip(raw_lines, 0, None)

        if is_display:
            plt.figure(figsize=(10, 10))
            plt.imshow(cv2.cvtColor(im_out, cv2.COLOR_BGR2RGB))
            plt.show()

        lines = list()
        with PyTessBaseAPI(psm=PSM.RAW_LINE, oem=OEM.LSTM_ONLY) as api:
            for raw_line in raw_lines:
                pt1 = (raw_line[0], raw_line[1])
                pt2 = (raw_line[2], raw_line[3])
                pt3 = (raw_line[6], raw_line[7])
                pt4 = (raw_line[4], raw_line[5])

                roated_im = self.tbd.dumpRotateImage(self.image, degrees(
                    atan2(pt2[1]-pt1[1], pt2[0]-pt1[0])), pt1, pt2, pt3, pt4)

                if is_display:
                    im = cv2.cvtColor(roated_im, cv2.COLOR_BGR2RGB)
                    plt.figure(figsize=(10, 10))
                    plt.imshow(im)
                    plt.show()

                pil_image = Image.fromarray(roated_im)
                api.SetImage(pil_image)
                text = api.GetUTF8Text()
                if is_display:
                    print("Detected text:", text)
                    print("Confidence:", api.AllWordConfidences())
                line = TextLine(text=text, box=raw_line)
                lines.append(line)

        return lines
