import ctpn
from ctpn.detectors import TextProposalDetector, TextDetector
from ctpn.other import CaffeModel, draw_boxes

import caffe
import cv2
from math import *
import numpy as np
import os

class TextBoxDetector():

    def __init__(self):
        self.text_detector = None

    def load(self, model_dir, use_gpu):
        deploy_path = os.path.join(model_dir, 'deploy.prototxt')
        if not os.path.isfile(deploy_path):
            raise RuntimeError(
                'No deploy file found in {0}'.format(deploy_path))
        model_path = os.path.join(model_dir, 'ctpn_trained_model.caffemodel')
        if not os.path.isfile(model_path):
            raise RuntimeError(
                'No model file found in {0}'.format(model_path))

        if use_gpu:
            caffe.set_mode_gpu()
            caffe.set_device(0)
        else:
            caffe.set_mode_cpu()

        self.text_proposals_detector = TextProposalDetector(
            CaffeModel(deploy_path, model_path))
        self.text_detector = TextDetector(self.text_proposals_detector)

    def getBoxes(self, im):
        if self.text_detector is None:
            raise RuntimeError('Model not loaded')
        tmp = im.copy()
        text_lines = self.text_detector.detect(im)
        text_recs, preview = draw_boxes(tmp, text_lines)
        return text_recs, preview

    def dumpRotateImage(self,img,degree,pt1,pt2,pt3,pt4):
        height,width=img.shape[:2]
        heightNew = int(width * fabs(sin(radians(degree))) + height * fabs(cos(radians(degree))))
        widthNew = int(height * fabs(sin(radians(degree))) + width * fabs(cos(radians(degree))))
        matRotation=cv2.getRotationMatrix2D((width/2,height/2),degree,1)
        matRotation[0, 2] += (widthNew - width) / 2
        matRotation[1, 2] += (heightNew - height) / 2
        imgRotation = cv2.warpAffine(img, matRotation, (widthNew, heightNew), borderValue=(255, 255, 255))
        pt1 = list(pt1)
        pt3 = list(pt3)
        [[pt1[0]], [pt1[1]]] = np.dot(matRotation, np.array([[pt1[0]], [pt1[1]], [1]]))
        [[pt3[0]], [pt3[1]]] = np.dot(matRotation, np.array([[pt3[0]], [pt3[1]], [1]]))
        imgOut=imgRotation[int(pt1[1]):int(pt3[1]),int(pt1[0]):int(pt3[0])]
        height,width=imgOut.shape[:2]
        return imgOut
