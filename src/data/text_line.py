from sympy.geometry import Polygon
import skimage.draw

import numpy as np

class TextLine():

    def __init__(self, box, text):
        self.box = box
        self.text = text.strip()
        self._polygon = Polygon(box[[1, 0]], box[[3, 2]], box[[7, 6]], box[[5, 4]])

    def polygon(self, clip):
        c = self.box[[0, 2, 6, 4]]
        r = self.box[[1, 3, 7, 5]]
        return skimage.draw.polygon(r, c, clip)

    def centroid(self):
        return int(self._polygon.centroid.x), int(self._polygon.centroid.y)

    def scale(self, ratio):
        ratio = ratio / 2
        box = self.box
        box[0] = box[0] * (100-ratio) / 100
        box[1] = box[1] * (100-ratio) / 100

        box[2] = box[2] * (100+ratio) / 100
        box[3] = box[3] * (100-ratio) / 100

        box[4] = box[4] * (100-ratio) / 100
        box[5] = box[5] * (100+ratio) / 100

        box[6] = box[6] * (100+ratio) / 100
        box[7] = box[7] * (100+ratio) / 100
        return TextLine(box=box, text=self.text)

    def __str__(self):
        return self.text
