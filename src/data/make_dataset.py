# -*- coding: utf-8 -*-
import os
import click
from datetime import datetime
import io
import json
import logging
from dotenv import find_dotenv, load_dotenv
import pytz
import shutil
from urllib import urlretrieve
from tqdm import tqdm

os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils


def to_timestamp(a_date):
    if a_date.tzinfo:
        epoch = datetime(1970, 1, 1, tzinfo=pytz.UTC)
        diff = a_date.astimezone(pytz.UTC) - epoch
    else:
        epoch = datetime(1970, 1, 1)
        diff = a_date - epoch
    return int(diff.total_seconds())


def get_date_range():
    dbegin_str = os.environ.get('DATE_RANGE_START')
    dend_str = os.environ.get('DATE_RANGE_END')

    dbegin = to_timestamp(datetime.min)
    if dbegin_str:
        dbegin = to_timestamp(datetime.strptime(dbegin_str, "%d/%m/%Y %Z"))

    dend = to_timestamp(datetime.max)
    if dend_str:
        dend = to_timestamp(datetime.strptime(dend_str, "%d/%m/%Y %Z"))

    return int(dbegin), int(dend)


def extract_names(ipath, opath, force):
    if not force and os.path.exists(opath):
        return

    with io.open(opath, 'w', encoding='utf-8') as of:
        for user in utils.read_json_dataset(ipath):
            s = u'{}, {}'.format(user['name'], user['screen_name'])
            of.write(s + u'\n')


def extract_images(ipath, opath, force):
    if not os.path.isdir(opath):
        os.mkdir(opath)

    dbegin, dend = get_date_range()

    with tqdm(total=utils.num_tweets(ipath)) as pbar:
        for tweet in tqdm(utils.read_json_dataset(ipath)):
            pbar.update(1)

            if tweet['created_at'] < dbegin or tweet['created_at'] > dend:
                continue

            url = None
            ofile = None
            try:
                tid = tweet['id']
                medias = tweet['entities']['media']
                for mid, media in enumerate(medias):
                    if media['type'] != 'photo':
                        continue
                    url = media['media_url']
                    ofile = os.path.join(opath, str(tid) + '_' + str(mid))
            except:
                pass
            if url and ofile:
                if not os.path.exists(ofile):
                    urlretrieve(url, ofile)


def group_tweets_by_username(ipath, opath, force):
    if not force and os.path.isdir(opath):
        return
    if os.path.isdir(opath):
        shutil.rmtree(opath)
    os.mkdir(opath)

    dbegin, dend = get_date_range()

    with tqdm(total=utils.num_tweets(ipath)) as pbar:
        for tweet in utils.read_json_dataset(ipath):
            pbar.update(1)
            try:
                screen_name = tweet['screen_name']
                if dbegin <= tweet['created_at'] <= dend:
                    with io.open(os.path.join(opath, screen_name), 'a', encoding='utf-8') as of:
                        of.write(json.dumps(tweet) + u'\n')
            except:
                pass


@click.command()
@click.argument('datadir', type=click.Path(exists=True))
@click.argument('force', default=False)
def main(datadir, force):
    logger = logging.getLogger(__name__)

    logger.info('Extracting names...')
    extract_names(os.path.join(datadir, 'raw', 'users.json'),
                  os.path.join(datadir, 'interim', 'names.txt'),
                  force)

    logger.info('Retrieving tweet images...')
    extract_images(os.path.join(datadir, 'raw', 'tweets.json'),
                   os.path.join(datadir, 'shared', 'img'),
                   force)

    logger.info('Grouping tweet by username...')
    group_tweets_by_username(os.path.join(datadir, 'raw', 'tweets.json'),
                             os.path.join(datadir, 'interim', 'tweets'),
                             force)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    load_dotenv(find_dotenv())

    main()
