from datetime import datetime
import os
from tempfile import mkstemp, mkdtemp
import unittest

import make_dataset as md


class TestUtils(unittest.TestCase):

    def test_get_date_range(self):
        os.environ['DATE_RANGE_START'] = '1/1/2005 UTC'
        os.environ['DATE_RANGE_END'] = '31/12/2005 UTC'
        dstart, dend = md.get_date_range()

        tstamp = md.to_timestamp(datetime.strptime('31/12/2004 UTC', "%d/%m/%Y %Z"))
        self.assertTrue(tstamp < dstart)
        tstamp = md.to_timestamp(datetime.strptime('1/1/2005 UTC', "%d/%m/%Y %Z"))
        self.assertTrue(tstamp >= dstart and tstamp <= dend)
        tstamp = md.to_timestamp(datetime.strptime('1/6/2005 UTC', "%d/%m/%Y %Z"))
        self.assertTrue(tstamp >= dstart and tstamp <= dend)
        tstamp = md.to_timestamp(datetime.strptime('31/12/2005 UTC', "%d/%m/%Y %Z"))
        self.assertTrue(tstamp >= dstart and tstamp <= dend)
        tstamp = md.to_timestamp(datetime.strptime('1/01/2006 UTC', "%d/%m/%Y %Z"))
        self.assertTrue(tstamp > dend)

    def test_extract_names(self):
        ipath = os.path.join(os.path.dirname(__file__), 'test_data', 'users.json')
        _, opath = mkstemp()
        md.extract_names(ipath, opath, True)
        self.assertEqual('Governor Bill Walker, AkGovBillWalker\n'
                         'Amy Klobuchar, amyklobuchar\n'
                         'Anthony G. Brown, AnthonyBrownMD4\n'
                         'Gov. Asa Hutchinson, AsaHutchinson\n', open(opath).read())
        md.extract_names(ipath, opath, False)
        self.assertEqual('Governor Bill Walker, AkGovBillWalker\n'
                         'Amy Klobuchar, amyklobuchar\n'
                         'Anthony G. Brown, AnthonyBrownMD4\n'
                         'Gov. Asa Hutchinson, AsaHutchinson\n', open(opath).read())

    def test_group_tweets_by_username(self):
        os.environ['DATE_RANGE_START'] = '1/1/2005 UTC'
        os.environ['DATE_RANGE_END'] = '31/12/2005 UTC'

        ipath = os.path.join(os.path.dirname(__file__), 'test_data', 'tweets.json')
        opath = mkdtemp()
        md.group_tweets_by_username(ipath, opath, True)

        self.assertEqual('{"text": "text1", "created_at": 1104537600, "user_id": 1, "id": 1, "screen_name": "name1"}\n'
                         '{"text": "text4", "created_at": 1104537600, "user_id": 1, "id": 4, "screen_name": "name1"}\n',
                         open(os.path.join(opath, 'name1')).read())
        self.assertEqual('{"text": "text2", "created_at": 1104537600, "user_id": 2, "id": 2, "screen_name": "name2"}\n'
                         '{"text": "text5", "created_at": 1104537600, "user_id": 2, "id": 5, "screen_name": "name2"}\n',
                         open(os.path.join(opath, 'name2')).read())
        self.assertEqual('{"text": "text3", "created_at": 1104537600, "user_id": 3, "id": 3, "screen_name": "name3"}\n'
                         '{"text": "text6", "created_at": 1104537600, "user_id": 3, "id": 6, "screen_name": "name3"}\n',
                         open(os.path.join(opath, 'name3')).read())


if __name__ == '__main__':
    unittest.main()
