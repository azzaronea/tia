class TextBlock(object):

    def __init__(self):
        self.lines = []

    def add_line(self, line):
        self.lines.append(line)

    # Get lines in order
    def get_lines(self):
        return sorted(self.lines, key=lambda line: line.centroid())

    def __str__(self):
        lines = self.get_lines()
        if len(lines) > 1:
            return reduce(lambda l1, l2: unicode(l1)+u'\n'+unicode(l2), lines)
        elif len(lines) == 1:
            return unicode(lines[0])
        else:
            return u""
