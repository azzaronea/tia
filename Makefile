.PHONY: clean data lint requirements sync_data_to_s3 sync_data_from_s3

#################################################################################
# GLOBALS                                                                       #
#################################################################################

CAFFE_PYTHON_DIR = $(shell realpath tmp/caffe/python)
PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
#BUCKET = [OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')
#PROFILE = default
PROJECT_NAME = tia
PYTHON_INTERPRETER = python

ifeq (,$(shell which conda))
HAS_CONDA=False
else
HAS_CONDA=True
endif

#################################################################################
# COMMANDS                                                                      #
#################################################################################

NEWDIR=$(shell date +%Y%m%d%H%M%S)
DATADIR=$(shell realpath data)
MODELDIR=$(shell realpath models)
REPORTDIR=$(shell realpath reports)

## Commit old data directory (if any) and crete a new one
new_data_experiment:
	mkdir -p  $(DATADIR)/experiments/$(NEWDIR)
	touch $(DATADIR)/experiments/current
	unlink $(DATADIR)/experiments/current
	ln -s $(DATADIR)/experiments/$(NEWDIR) $(DATADIR)/experiments/current
	mkdir $(DATADIR)/experiments/current/interim
	mkdir $(DATADIR)/experiments/current/processed
	ln -s $(DATADIR)/external $(DATADIR)/experiments/current/external
	ln -s $(DATADIR)/raw $(DATADIR)/experiments/current/raw
	cp .env $(DATADIR)/experiments/current/

	ln -s  $(DATADIR)/shared/ $(DATADIR)/experiments/current/shared

## Commit old model directory (if any) and crete a new one
new_model_experiment:
	mkdir -p  $(MODELDIR)/experiments/$(NEWDIR)
	touch $(MODELDIR)/experiments/current
	unlink $(MODELDIR)/experiments/current
	ln -s $(MODELDIR)/experiments/$(NEWDIR) $(MODELDIR)/experiments/current
	ln -s $(MODELDIR)/external $(MODELDIR)/experiments/current/external
	cp .env $(MODELDIR)/experiments/current/
	mkdir $(MODELDIR)/experiments/current/img
	mkdir $(MODELDIR)/experiments/current/normal

## Commit old report directory (if any) and crete a new one
new_report_experiment:
	mkdir -p  $(REPORTDIR)/experiments/$(NEWDIR)
	touch $(REPORTDIR)/experiments/current
	unlink $(REPORTDIR)/experiments/current
	ln -s $(REPORTDIR)/experiments/$(NEWDIR) $(REPORTDIR)/experiments/current
	cp .env $(REPORTDIR)/experiments/current/
	mkdir $(REPORTDIR)/experiments/current/img
	mkdir $(REPORTDIR)/experiments/current/normal

## Commit old experiment (if any) and crete a new one
new_experiment: new_data_experiment new_model_experiment new_report_experiment

## Install Python Dependencies
requirements: test_environment
	pip install -r requirements.txt

## Make Dataset
data: requirements
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/data/make_dataset.py data/experiments/current/

data_force: requirements
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/data/make_dataset.py data/experiments/current/ 1

extract_text: data
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	export PYTHONPATH=$(PROJECT_DIR)/tmp/caffe/python:$(PYTHONPATH) && $(PYTHON_INTERPRETER) src/data/extract_text.py data/experiments/current/

extract_text_only:
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	export PYTHONPATH=$(PROJECT_DIR)/tmp/caffe/python:$(PYTHONPATH) && $(PYTHON_INTERPRETER) src/data/extract_text.py data/experiments/current/

## Make Features
features: data
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/features/build_features.py data/experiments/current/ models/experiments/current

features_force: data
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/features/build_features.py data/experiments/current/ models/experiments/current 1

## Make Features (using also text from images)
features_using_images_only:
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/features/build_features_using_images.py data/experiments/current/ models/experiments/current

features_using_images: data extract_text
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/features/build_features_using_images.py data/experiments/current/ models/experiments/current

features_using_images_force: data extract_text
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/features/build_features_using_images.py data/experiments/current/ models/experiments/current 1

## Train NLP model
train_model: features
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/models/train_model.py data/experiments/current/ models/experiments/current reports/experiments/current/

train_model_using_images: features_using_images
	cp .env $(DATADIR)/experiments/current/
	cp .env $(MODELDIR)/experiments/current/
	cp .env $(REPORTDIR)/experiments/current/
	$(PYTHON_INTERPRETER) src/models/train_model_using_images.py data/experiments/current/ models/experiments/current reports/experiments/current/


## Delete all compiled Python files
clean:
	find . -name "*.pyc" -exec rm {} \;

## Lint using flake8
lint:
	flake8 --exclude=lib/,bin/,docs/conf.py,tmp/,src/data/ctpn .

test:
	$(PYTHON_INTERPRETER) src/test.py
	$(PYTHON_INTERPRETER) src/data/test.py
	$(PYTHON_INTERPRETER) src/features/test.py

# Build caffe
caffe: requirements
	if cd tmp/caffe; then git pull; else cd tmp && git clone https://github.com/azzar1/caffe caffe; fi
	cp tmp/caffe/Makefile.config.example tmp/caffe/Makefile.config
	cd tmp/caffe &&  make -j 4 && make pycaffe

## Build ctpnx
ctpn: caffe
	cd src/data/ctpn && $(MAKE)

## Upload Data to S3
# sync_data_to_s3:
# ifeq (default,$(PROFILE))
# 	aws s3 sync data/ s3://$(BUCKET)/data/
# else
# 	aws s3 sync data/ s3://$(BUCKET)/data/ --profile $(PROFILE)
# endif

## Download Data from S3
# sync_data_from_s3:
# ifeq (default,$(PROFILE))
# 	aws s3 sync s3://$(BUCKET)/data/ data/
# else
# 	aws s3 sync s3://$(BUCKET)/data/ data/ --profile $(PROFILE)
# endif

## Set up python interpreter environment
create_environment:
ifeq (True,$(HAS_CONDA))
		@echo ">>> Detected conda, creating conda environment."
ifeq (3,$(findstring 3,$(PYTHON_INTERPRETER)))
	conda create --name $(PROJECT_NAME) python=3
else
	conda create --name $(PROJECT_NAME) python=2.7
endif
		@echo ">>> New conda env created. Activate with:\nsource activate $(PROJECT_NAME)"
else
	@pip install --user -q virtualenv virtualenvwrapper
	@echo ">>> Installing virtualenvwrapper if not already intalled.\nMake sure the following lines are in shell startup file\n\
	export WORKON_HOME=$$HOME/.virtualenvs\nexport PROJECT_HOME=$(shell pwd)\nsource $(shell which virtualenvwrapper.sh)\n"
	@bash -c "source `which virtualenvwrapper.sh`;mkvirtualenv $(PROJECT_NAME) --python=$(PYTHON_INTERPRETER)"
	@echo ">>> New virtualenv created. Activate with:\nworkon $(PROJECT_NAME)"
endif

## Test python environment is setup correctly
test_environment:
	$(PYTHON_INTERPRETER) test_environment.py

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := show-help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: show-help
show-help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
